from telebot.types import (
    InlineKeyboardMarkup,
    InlineKeyboardButton
)

def get_languages():
    keyboard = InlineKeyboardMarkup(row_width=1)
    keyboard.add(
        InlineKeyboardButton(text='English', callback_data='en'),
        InlineKeyboardButton(text='Русский', callback_data='ru'),
        InlineKeyboardButton(text='Кыргызча', callback_data='kg'),
    )
    return keyboard