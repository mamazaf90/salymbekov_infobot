import json

def get_text(text_key, language):
    with open("db/text.json", "r") as file:
        text_dict = json.loads(file.read())
    return text_dict[text_key][language]